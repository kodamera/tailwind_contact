# Tailwind Contact Module

### Installation
Add following section to your `composer.json`
```
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/kodamera/tailwind_contact.git"
    }
],
```
From terminal run `$ composer require kodamera/tailwind_contact` to require module and then activate module as you normally would do.
